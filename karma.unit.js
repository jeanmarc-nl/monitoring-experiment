const webpackConfig = require('./webpack.config.js');
webpackConfig.entry = {};
webpackConfig.output = {};
module.exports = function(config){
  config.set({
    basepath: '',
    frameworks: ['jasmine'],
    files: [
        './src/test/**/*.test.js'
    ],
    exclude: [
    ],
    preprocessors: {
      './dist/main.bundle.js': ['webpack'],
      './src/test/**/*.test.js': ['webpack']
    },
    webpack: webpackConfig,

    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeHeadless'],
    singleRun: true,
    concurrency: Infinity,
    mime: {
      'text/x-typescript':  ['ts', 'tsx']
    }

  })
}