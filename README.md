# Monitoring Experiment

Experimenting with graphical presentation of infrastructure and its metrics.

Basic building block: blocks with 1 ground plane (attached to a parent), 4 sides, and 1 top plane

Ground plane has dimensions (what matters is the ratio between x and y, since all displaying will be scalable)
Sides height is a factor of the ground size (either the x, y, or diagonal of the ground plane)

Sides and top have a grid (anywhere between 1x1 and nxm) and each grid cell can receive a child

Grid cells have padding (fixed amount on all sides to start) (eventually multiple options like either fixed pixels or percentage of their dimensions)

Blocks have a single color on all sides (side color can be influenced by placing a flat child on a side in a 1x1 grid)

Children are allowed to be placed on any rectangular group of grid cells

## Playground setup
To develop locally, make sure you have Docker installed and running, then run `docker-compose up` to spin up a 
Prometheus instance that will act as the source for all metrics on display.
The docker-compose will also start some auxiliary services that help development and experimenting:
* a small simulator that produces several metrics (not there yet)
* a Grafana instance to visualize the Prometheus metrics on traditional 2d dashboards (for comparison)

The first time you access Grafana (http://localhost:3000), you need to login (`admin`/`justwork`) and add
Prometheus as a source. Use address `http://localhost:9090` and type `Browser`, then press `Test & Save`.

## Initializing the local application
`npm install` should set you up.

`npm run start` will call webpack to generate the application in `./dist`, and starts a server on `localhost:8080` to
show the application. The command will monitor for file changes and rebuild and reload on every save.

If the browser cache needs to be bypassed, reload the page with `Shift-Command-R`.

`npm run test` will start Karma tests and monitors changes to the test and source files, re-testing on every change.

## Example Prometheus queries
* Counter
    * http://localhost:9090/api/v1/query?query=go_memstats_alloc_bytes_total
* Gauge
    * http://localhost:9090/api/v1/query?query=go_memstats_buck_hash_sys_bytes
* Summary
    * main metric provides buckets
    * also has `_sum` and `_count` counters
    * http://localhost:9090/api/v1/query?query=go_gc_duration_seconds
* Histogram
    * main metric with `_bucket` suffix provides buckets
    * also has `_sum` and `_count` counters
    * http://localhost:9090/api/v1/query?query=prometheus_http_request_duration_seconds_bucket

Each query delivers a json with a similar structure:
```json
{
  "status": "success",
  "data": {
    "resultType": "vector",
    "result": [
      ...
    ]
  }
}
```
The result elements are structures like this:
```json
{
  "metric": {
    "__name__": "[metric name]",
    "instance": "[prometheus server name:port]",
    "job": "prometheus",
    ... [more tags]
  },
  "value": [ timestamp, "[value]"]
}
```
For `summary` and `histogram` type metrics there are special tags to indicate the bucket:
```json
...summary metric:
"quantile": "[quantile value]", 

...histogram metric:
"le": "[less or equal value]",

```
`quantile` provides a division in percentage (range 0..1) and each value represents the value for 0..X

`le` provides a division in buckets, and each value represents the value for -Inf..X

To get the value for bucket N, one needs to subtract the value (N-1).

## Prometheus-Query objects
The queries are translated to objects with a slightly different structure compared to the raw prometheus responses:

```json
{
  "resultType": "vector",
  "result": [
    {
      "metric": {
        "name": "prometheus_tsdb_compaction_chunk_size_bytes_bucket",
        "labels": {
          "instance": "localhost:9090",
          "job": "prometheus",
          "le": "+Inf"
        }
      },
      "value": {
        "time": "2021-04-09T11:52:40.116Z",
        "value": 16058
      }
    },
...
```
