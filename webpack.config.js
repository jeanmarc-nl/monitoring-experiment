const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

function format(num, pad = 2){
  return String(num).padStart(pad, '0');
}

class WatchTimerPlugin {
  apply(compiler) {
    compiler.hooks.done.tap('Watch Timer Plugin', (stats) => {
      setTimeout(
          () => {
            const now = new Date();
            const nowString = `${now.getFullYear()}-${format(now.getMonth() + 1)}-${format(now.getDate())} ${format(now.getHours())}:${format(now.getMinutes())}:${format(now.getSeconds())}.${format(now.getMilliseconds(), 3)}`;
            console.log(`[${nowString}] Done.`);
          }, 100)
    });
  }
}

module.exports = {
  mode: 'development',
  entry: './src/index.ts',
  devtool: 'inline-source-map',
  output: {
    filename: "[name].bundle.js",
    path: path.resolve(__dirname, 'dist'),
    assetModuleFilename: 'static/[hash][ext][query]'
  },
  optimization: {
    runtimeChunk: 'single',
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      title: 'ThreeJS Metrics Monitor',
      favicon: './src/resources/favicon.ico'
    }),
    new WatchTimerPlugin()
  ],
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '...'],
    roots: [
        path.resolve('./src/modules'),
        path.resolve('./src/resources'),
        path.resolve('./src')
    ]
  },
  devServer: {
    writeToDisk: true
  },
  watch: true,
  watchOptions: {
    aggregateTimeout: 200,
    //poll: 1000,
    ignored: [ '**/node_modules/' ]
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
        generator: {
          filename: 'images/[name][ext][query]'
        }
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: 'asset/resource',
        generator: {
          filename: 'resources/[name][ext][query]'
        }
      },
      {
        test: /\.js$/,
        enforce: 'pre',
        use: ['source-map-loader']
      }
    ]
  }
};
