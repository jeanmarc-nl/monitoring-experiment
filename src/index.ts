import * as THREE from 'three';
import Stats from 'three/examples/jsm/libs/stats.module.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';

import { GUI } from 'dat.gui';

import { createView } from './modules/mainview';

import './main.css';
import {addDemoContent, updateDemoScene} from "./modules/sampleContent";
import {addBrickContent, updateBrickContent} from "./modules/brickdemo";

function main(){

  function onWindowResize(){
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
  }

  function toggleConsole(){
    if (showConsole) {
      messages.classList.add('show');
      messages.classList.remove('hide');
    } else {
      messages.classList.add('hide');
      messages.classList.remove('show');
    }
  }

  function onKeyDown(event: KeyboardEvent){
    if (event.key == 'c'){
      showConsole = !showConsole;
      toggleConsole();
      messages.innerText += 'Console toggled from typescript.\n';
    }
  }

  const scene = new THREE.Scene();
  const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
  const gui = new GUI();
  const renderer = new THREE.WebGLRenderer({antialias: true});
  renderer.setSize( window.innerWidth, window.innerHeight );
  renderer.setClearColor( 0x111122, 1);

  window.addEventListener( 'resize', onWindowResize, false );

  createView(document.body, renderer.domElement);

  addBrickContent(renderer, scene, camera, gui);


  camera.position.z = 3.5;

  const controls = new OrbitControls( camera, renderer.domElement );
  controls.target.y = 0;

  // controls.update() must be called after any manual changes to the camera's transform
  controls.update();

  addDemoContent(renderer, scene, camera, controls);

  const stats: Stats = Stats();
  stats.showPanel(1);
  document.body.appendChild(stats.dom);
  const messages = document.createElement('div');
  messages.setAttribute('id', 'messages');
  messages.setAttribute('class', 'console');

  let showConsole: boolean = false;
  toggleConsole();

  document.body.appendChild(messages);
  document.addEventListener('keydown', onKeyDown);

  const clock = new THREE.Clock();

  const maxDelta = 0.1; // 100 ms
  const animate = function () {
    requestAnimationFrame( animate );

    const delta = Math.min(clock.getDelta(), maxDelta);
    updateDemoScene(delta);
    updateBrickContent(delta);

    //controls.update();

    stats.update();
    renderer.render( scene, camera );
  };

  animate();
}

main();
