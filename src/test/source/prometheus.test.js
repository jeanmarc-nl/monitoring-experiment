import {Gauge} from "../../modules/data/gauge";
import {PrometheusSource} from "../../modules/source/prometheus";

describe('Prometheus source', () => {
  it('Should retrieve a gauge value', done => {
    const prometheus = new PrometheusSource("http://localhost:9090");
    function callback(data) {
      try {
        expect(data).not.toEqual(42);
        done();
      } catch(error) {
        done(error);
      }
    }
    prometheus.queryGauge("go_memstats_heap_objects", callback);
  })
  it('Should retrieve a counter value', done => {
    const prometheus = new PrometheusSource("http://localhost:9090");
    function callback(data) {
      try {
        expect(data).not.toEqual(42);
        done();
      } catch(error) {
        done(error);
      }
    }
    prometheus.queryCounter("go_memstats_alloc_bytes_total", callback);
  })
})
