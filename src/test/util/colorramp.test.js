import {RampFull, RampPBGR, RampTemperature} from "../../modules/util/colorramp";

describe('Color ramps', () => {
  it('Should work', () => {
    let col = RampFull(0);
    expect(col.r).toEqual(0);
    col = RampPBGR(1);
    expect(col.r).toEqual(1);
    col = RampPBGR(0);
    expect(col.r).toEqual(1);
    col = RampTemperature(0);
    expect(col.r).toEqual(0.1);
    expect(col.g).toEqual(0.1);
    expect(col.b).toEqual(0.1);
  })
})
