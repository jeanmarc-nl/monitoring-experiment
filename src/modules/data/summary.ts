import {QueryResult} from 'prometheus-query';
import {PrometheusSource} from "../source/prometheus";

export {SummaryValue, Summary}

class SummaryValue {
    public time: Date;
    public bucket: number;
    public value: number;

    constructor(time: Date, bucket: number, value: number) {
        this.time = time;
        this.bucket = bucket;
        this.value = value;
    }
}

class Summary {
    private _current?: QueryResult;
    private _source?: PrometheusSource;

    setSource(source: PrometheusSource, query: string, queryIntervalMS = 1000) {
        this._source = source;
        setInterval(this.tick, queryIntervalMS, this, query)
    }

    // return array of SummaryValue objects
    read(): SummaryValue[] {
        const result = new Array();
        if (this._current !== undefined) {
            this._current.result.forEach(el => {
                result.push(new SummaryValue(el.value.time, el.metric.labels.quantile, el.value.value));
            });
            // sort the array based on bucket value
            result.sort(compare);
        }
        return result;
    }

    tick(self: Summary, query: string) {
        if (self._source !== undefined) {
            self._source.querySummary(query, self.update, self);
        }
    }

    update(results: QueryResult, self = this) {
        self._current = Object.assign({}, results);
    }

}

function compare(a: SummaryValue, b: SummaryValue) {
    return a.bucket - b.bucket;
}
