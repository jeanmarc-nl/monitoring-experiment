import {PrometheusSource} from "../source/prometheus";

export {Counter, CounterValue}


class CounterValue {
    public min: number;
    public max: number;
    public value: number;

    constructor(min: number, max: number, value: number) {
        this.min = min;
        this.max = max;
        this.value = value;
    }
}

class Counter {
    private _current: CounterValue;
    private _source?: PrometheusSource;

    constructor(initial = 0) {
        this._current = new CounterValue(initial, initial, initial);
    }

    input(value: number, self = this) {
        self._current.value = value;
        self._current.min = Math.min(self._current.min, value);
        self._current.max = Math.max(value, self._current.max);
    }

    read() {
        return new CounterValue(this._current.min, this._current.max, this._current.value);
    }

    setSource(source: PrometheusSource, query: string, queryIntervalMS = 1000) {
        this._source = source;
        setInterval(this.tick, queryIntervalMS, this, query)
    }

    tick(self: Counter, query: string) {
        if (self._source !== undefined) {
            self._source.queryCounter(query, self.input, self);
        }
    }
}
