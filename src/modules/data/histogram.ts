import {QueryResult} from 'prometheus-query';
import {PrometheusSource} from "../source/prometheus";

export {HistogramValue, Histogram}

class HistogramValue {
    public time: Date;
    public bucket: string;
    public value: number;

    constructor(time: Date, bucket: string, value: number) {
        this.time = time;
        this.bucket = bucket;
        this.value = value;
    }
}

class Histogram {
    private _current?: QueryResult;
    private _source?: PrometheusSource;

    setSource(source: PrometheusSource, query: string, queryIntervalMS = 1000) {
        this._source = source;
        setInterval(this.tick, queryIntervalMS, this, query)
    }

    // return array of HistogramValue objects
    read(): HistogramValue[] {
        const result = new Array();
        if (this._current !== undefined) {
            this._current.result.forEach(el => {
                result.push(new HistogramValue(el.value.time, el.metric.labels.le, el.value.value));
            });
            // sort the array based on bucket name
            result.sort(compare);
        }
        return result;
    }

    tick(self: Histogram, query: string) {
        if (self._source !== undefined) {
            self._source.queryHistogram(query, self.update, self);
        }
    }

    update(results: QueryResult, self = this) {
        self._current = Object.assign({}, results);
    }

}

function compare(a: HistogramValue, b: HistogramValue) {
    if (a.bucket == b.bucket) {
        return 0;
    }
    // First tackle the '+Inf' bucket
    if (a.bucket == '+Inf') {
        return 1;
    }
    if (b.bucket == '+Inf') {
        return -1;
    }
    // Then sort by numeric value
    const aVal = parseFloat(a.bucket);
    const bVal = parseFloat(b.bucket);
    if (aVal > bVal) {
        return 1;
    }
    if (aVal < bVal) {
        return -1;
    }
    return 0;
}
