import {PrometheusSource} from "../source/prometheus";

export {Gauge, GaugeValue}

class GaugeValue{
    public min: number;
    public max: number;
    public value: number;
  constructor(min: number, max: number, value: number) {
    this.min = min;
    this.max = max;
    this.value = value;
  }
}

class Gauge {
    private _current: GaugeValue;
    private _autoLimits: boolean;
    private _source?: PrometheusSource;
  constructor(min = 0, max = min, initial = min, autoLimits = true) {
    this._autoLimits = autoLimits;
    this._current = new GaugeValue(min, max, initial);
    this.input(initial);
  }

  input(value: number, self = this){
    if (self._autoLimits){
      self._current.min = Math.min(self._current.min, value);
      self._current.max = Math.max(self._current.max, value);
      self._current.value = value;
    } else {
      self._current.value = Math.max(Math.min(value, self._current.max), self._current.min);
    }
  }

  read(){
    return new GaugeValue(this._current.min, this._current.max, this._current.value);
  }

  setSource(source: PrometheusSource, query: string, queryIntervalMS = 1000){
    this._source = source;
    setInterval(this.tick, queryIntervalMS, this, query)
  }

  tick(self: Gauge, query: string){
      if (self._source !== undefined) {
          self._source.queryGauge(query, self.input, self);
      }
  }
}
