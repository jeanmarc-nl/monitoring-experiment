import {PrometheusDriver, QueryResult} from 'prometheus-query';
import {Gauge} from "../data/gauge";
import {Counter} from "../data/counter";
import {Histogram} from "../data/histogram";
import {Summary} from "../data/summary";

export {PrometheusSource}

type CounterCallback = (val: number, obj: Counter) => void;
type GaugeCallback = (val: number, obj: Gauge) => void;
type HistogramCallback = (val: QueryResult, obj: Histogram) => void;
type SummaryCallback = (val: QueryResult, obj: Summary) => void;

class PrometheusSource {
    private _endpoint: string;
    private _baseUrl: string;
    private _driver: PrometheusDriver;
    constructor(endpoint: string, baseUrl = "/api/v1") {
        this._endpoint = endpoint;
        this._baseUrl = baseUrl;
        this._driver = new PrometheusDriver({
            endpoint: this._endpoint,
            baseURL: this._baseUrl
        });

    }

    queryCounter(q: string, callback: CounterCallback, counter: Counter) {
        this._driver.instantQuery(q)
            .then((res) => {
                const value = parseFloat(res.result[0].value.value);
                callback(value, counter);
            })

    }

    queryGauge(q: string, callback: GaugeCallback, gauge: Gauge) {
        this._driver.instantQuery(q)
            .then((res) => {
                const value = parseFloat(res.result[0].value.value);
                callback(value, gauge);
            })
            .catch(console.error);
    }

    queryHistogram(q: string, callback: HistogramCallback, histogram: Histogram) {
        this._driver.instantQuery(q)
            .then((res) => {
                callback(res, histogram);
            })
            .catch(console.error);
    }

    querySummary(q: string, callback: SummaryCallback, summary: Summary) {
        this._driver.instantQuery(q)
            .then((res) => {
                callback(res, summary);
            })
            .catch(console.error);
    }

}