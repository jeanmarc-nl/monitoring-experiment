import * as THREE from 'three'
import {GUI} from 'dat.gui';

import {Brick, BrickDimension} from "./brick/brick";
import {CarouselLeaf} from "./brick/carouselLeaf";
import {GraphicLeaf} from "./brick/graphicleaf";
import {HistogramLeaf} from "./brick/histogramLeaf";
import {GaugeLeaf} from "./brick/gaugeleaf";
import {Grid, GridDimension} from "./brick/grid";
import {GLOBAL} from "./brick/global";
import {Visual} from "./brick/visual";
import {Gauge} from "./data/gauge";

import {Histogram} from "./data/histogram";
import {PrometheusSource} from "./source/prometheus";
import {LoadTexture} from "./util/textures";

import {V3} from './util/vectors'
import {Summary} from "./data/summary";
import {SummaryLeaf} from "./brick/summaryLeaf";

export {addBrickContent, updateBrickContent}

let TXmetricBars = require('../resources/images/metrics_bar.png');
let TXmetric8 = require('../resources/images/metrics_8.png');
let TXmetric42 = require('../resources/images/metrics_42.png');

let rootBricks: Visual[] = [];

function addBrickContent(renderer: THREE.WebGLRenderer, scene: THREE.Scene, camera: THREE.Camera, gui: GUI) {
    const p1 = new THREE.Vector3(-4, 2, 0);
    const p2 = new THREE.Vector3(6, 2, 2);
    const p3 = new THREE.Vector3(4, 2, 12);
    const p4 = new THREE.Vector3(-6, 2, 10);
    const r1 = new THREE.Vector3(1, 0, 1);
    const r2 = new THREE.Vector3(0, 0, 1);
    const r3 = new THREE.Vector3(-1, 0, 0);
    const r4 = new THREE.Vector3(0, 0, -1);

    const b1 = makeDemoBrick(scene, p1, GLOBAL.up, r1);

    const b2 = makeDemoBrick(scene, p2, GLOBAL.up, r2);
    b2.visualObject.rotation.y = -Math.PI / 2;
    const b3 = makeDemoBrick(scene, p3, GLOBAL.up, r3);
    b3.visualObject.rotation.y = -Math.PI;
    const b4 = makeDemoBrick(scene, p4, GLOBAL.up, r4);
    b4.visualObject.rotation.y = -3 * Math.PI / 2;

    const gauge = new Gauge(0, 10, 2);
    const prometheus = new PrometheusSource("http://localhost:9090");
    gauge.setSource(prometheus, "go_memstats_heap_objects")

    const gauge2 = new Gauge(0, 10, 2);
    gauge2.setSource(prometheus, "go_memstats_mspan_inuse_bytes")

    b1.addChild(new V3(1, 6, GLOBAL.BP_UP), new V3(1, 1, 1), new GaugeLeaf(gauge));
    b1.addChild(new V3(3, 6, GLOBAL.BP_UP), new V3(1, 1, 1), new GaugeLeaf(gauge2));

    const histogram = new Histogram();
    histogram.setSource(prometheus, "prometheus_tsdb_compaction_chunk_size_bytes_bucket")

    const histogramLeaf = new HistogramLeaf(histogram, new THREE.Color(0.8, 0.8, 0.2));
    b1.addChild(new V3(4, 1, GLOBAL.BP_SOUTH), new V3(4, 2, 1), histogramLeaf);

    const summary = new Summary();
    summary.setSource(prometheus, "go_gc_duration_seconds")

    const summaryLeaf = new SummaryLeaf(summary, new THREE.Color(0.8, 0.2, 0.8));
    b1.addChild(new V3(2, 3, GLOBAL.BP_SOUTH), new V3(2, 1, 1), summaryLeaf);

}

function makeDemoBrick(scene: THREE.Scene, origin: THREE.Vector3, up: THREE.Vector3, right: THREE.Vector3) {
    const dims = new BrickDimension(8, 4, 8);
    const root = new Brick(dims, origin, up, right);

    const carouselTextures = [
        LoadTexture(randomTexture()),
        LoadTexture(randomTexture()),
        LoadTexture(randomTexture())
    ];
    root.addChild(new V3(4, 3, GLOBAL.BP_SOUTH), new V3(4, 1, 0), new CarouselLeaf(carouselTextures));


    const txt1 = LoadTexture(randomTexture());
    const txt2 = LoadTexture(randomTexture());
    root.addChild(new V3(0, 0, GLOBAL.BP_SOUTH), new V3(4, 1, 0), new GraphicLeaf(txt1));
    root.addChild(new V3(4, 0, GLOBAL.BP_SOUTH), new V3(4, 1, 0), new GraphicLeaf(txt2));

    const gd = new GridDimension(8, 4);
    const g1 = new Grid(gd);
    root.addChild(new V3(0, 1, GLOBAL.BP_SOUTH), new V3(4, 2, 0), g1);

    const tx1 = LoadTexture(randomTexture());
    const tx2 = LoadTexture(randomTexture());
    const tx3 = LoadTexture(randomTexture());
    const tx4 = LoadTexture(randomTexture());
    const tx5 = LoadTexture(randomTexture());
    const tx6 = LoadTexture(randomTexture());
    const tx7 = LoadTexture(randomTexture());
    const tx8 = LoadTexture(randomTexture());
    g1.addChild(new V3(0, 0, GLOBAL.BP_SOUTH), new V3(4, 1, 0), new GraphicLeaf(tx1));
    g1.addChild(new V3(0, 1, GLOBAL.BP_SOUTH), new V3(4, 1, 0), new GraphicLeaf(tx2));
    g1.addChild(new V3(0, 2, GLOBAL.BP_SOUTH), new V3(4, 1, 0), new GraphicLeaf(tx3));
    g1.addChild(new V3(0, 3, GLOBAL.BP_SOUTH), new V3(4, 1, 0), new GraphicLeaf(tx4));
    g1.addChild(new V3(4, 0, GLOBAL.BP_SOUTH), new V3(4, 1, 0), new GraphicLeaf(tx5));
    g1.addChild(new V3(4, 1, GLOBAL.BP_SOUTH), new V3(4, 1, 0), new GraphicLeaf(tx6));
    g1.addChild(new V3(4, 2, GLOBAL.BP_SOUTH), new V3(4, 1, 0), new GraphicLeaf(tx7));
    g1.addChild(new V3(4, 3, GLOBAL.BP_SOUTH), new V3(4, 1, 0), new GraphicLeaf(tx8));

    const tx9 = LoadTexture(randomTexture());
    root.addChild(new V3(0, 2, GLOBAL.BP_UP), new V3(8, 2, 0), new GraphicLeaf(tx9));
    const tx10 = LoadTexture(randomTexture());
    const tx11 = LoadTexture(randomTexture());
    root.addChild(new V3(0, 0, GLOBAL.BP_EAST), new V3(8, 2, 0), new GraphicLeaf(tx10));
    root.addChild(new V3(0, 2, GLOBAL.BP_EAST), new V3(8, 2, 0), new GraphicLeaf(tx11));
    const tx12 = LoadTexture(randomTexture());
    const tx13 = LoadTexture(randomTexture());
    root.addChild(new V3(0, 0, GLOBAL.BP_WEST), new V3(8, 2, 0), new GraphicLeaf(tx12));
    root.addChild(new V3(0, 2, GLOBAL.BP_WEST), new V3(8, 2, 0), new GraphicLeaf(tx13));
    const tx14 = LoadTexture(randomTexture());
    const tx15 = LoadTexture(randomTexture());
    root.addChild(new V3(0, 0, GLOBAL.BP_NORTH), new V3(8, 2, 0), new GraphicLeaf(tx14));
    root.addChild(new V3(0, 2, GLOBAL.BP_NORTH), new V3(8, 2, 0), new GraphicLeaf(tx15));

    rootBricks.push(root);
    scene.add(root.visualObject);

    return root;
}

function updateBrickContent(timeDelta: number) {
    rootBricks.forEach(function (obj) {
        obj.update(timeDelta)
    });
}

function randomTexture() {
    const rnd = Math.random();
    if (rnd < 0.33) {
        return TXmetric8
    } else if (rnd < 0.67) {
        return TXmetric42
    } else {
        return TXmetricBars
    }
}
