
export function createView(domElement: HTMLElement, rendererDom: HTMLCanvasElement){
  const titleElement = document.createElement('div');

  titleElement.innerHTML = '<= Three JS Experimental Metrics Display >=';
  titleElement.classList.add('title');

  domElement.appendChild(titleElement);
  domElement.appendChild(rendererDom);

}
