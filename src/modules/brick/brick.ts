import * as THREE from 'three'
import {GLOBAL} from "./global"
import {Grid, GridDimension} from "./grid"
import {Visual} from './visual'
import {LoadTexture} from "../util/textures"
import {V3} from '../util/vectors'

export {Brick, BrickDimension};

let EmptyBrick = require('../../resources/images/empty_brick.png');

class BrickDimension {
    public grid: V3;
    public cell: V3;

    constructor(cols: number, rows: number, slices: number, cellWidth = 1, cellHeight = 1, cellDepth = 1) {
        this.grid = new V3(cols, rows, slices);
        this.cell = new V3(cellWidth, cellHeight, cellDepth);
    }

    getSize() {
        return new THREE.Vector3(this.grid.width * this.cell.width, this.grid.height * this.cell.height, this.grid.depth * this.cell.depth);
    }

    clone() {
        return new BrickDimension(this.grid.width, this.grid.height, this.grid.depth, this.cell.width, this.cell.height, this.cell.depth);
    }
}

interface BrickFaces {
    north: Grid,
    east: Grid,
    south: Grid,
    west: Grid,
    up: Grid
}

/*
    A Brick has 5 child faces (upright faces facing North, East, South, and West, and one facing upwards)
    The default orientation is North == negative Z, East == positive X, and Up == Positive Y
    The default location is such that one corner of the brick sits at the Origin
    There are three faces that share this corner:
    South face top left corner,
    West face top right corner,
    Up face bottom left corner

    Each face of the Brick is divided into a grid (based on the width, height, and depth), and the grid
    coordinates are zero based, with the origin at the top left of the face

    The root brick uses 1 as the width/height/depth of a grid cell, and child nodes will be scaled to fit in their
    allocated grid-subsection (not preserving aspect ratio).
 */
class Brick extends Visual {
    public dimension: BrickDimension;
    public faces: BrickFaces;

    constructor(brickDim: BrickDimension, origin = GLOBAL.origin, up = GLOBAL.up, right = GLOBAL.right) {
        super(origin, up, right);
        this.dimension = brickDim.clone();

        const dims_NS = new GridDimension(brickDim.grid.width, brickDim.grid.height, brickDim.cell.width, brickDim.cell.height);
        const dims_EW = new GridDimension(brickDim.grid.depth, brickDim.grid.height, brickDim.cell.depth, brickDim.cell.height);
        const dims_UP = new GridDimension(brickDim.grid.width, brickDim.grid.depth, brickDim.cell.width, brickDim.cell.depth);

        const size = brickDim.getSize();

        const originNorth = new THREE.Vector3(size.x, 0, -size.z);
        const originEast = new THREE.Vector3(size.x, 0, 0);
        const originSouth = GLOBAL.origin;
        const originWest = new THREE.Vector3(0, 0, -size.z);
        const originUp = new THREE.Vector3(0, 0, -size.z);

        const rightNorth = right.clone();
        const rightEast = right.clone();
        const rightSouth = right.clone();
        const rightWest = right.clone();
        const rightUp = right.clone();

        const upNorth = up.clone();
        const upEast = up.clone();
        const upSouth = up.clone();
        const upWest = up.clone();
        const upUp = up.clone();

        this.faces = {
            north: new Grid(dims_NS, originNorth, upNorth, rightNorth),
            east: new Grid(dims_EW, originEast, upEast, rightEast),
            south: new Grid(dims_NS, originSouth, upSouth, rightSouth),
            west: new Grid(dims_EW, originWest, upWest, rightWest),
            up: new Grid(dims_UP, originUp, upUp, rightUp),
        }
    }

    addChild(position: V3, size: V3, child: Visual) {
        switch (position.z) {
            case GLOBAL.BP_NORTH:
                this.faces.north.addChild(position, size, child);
                break;
            case GLOBAL.BP_EAST:
                this.faces.east.addChild(position, size, child);
                break;
            case GLOBAL.BP_SOUTH:
                this.faces.south.addChild(position, size, child);
                break;
            case GLOBAL.BP_WEST:
                this.faces.west.addChild(position, size, child);
                break;
            case GLOBAL.BP_UP:
                this.faces.up.addChild(position, size, child);
                break;
        }
    }

    _getVisual() {
        const width = this.dimension.grid.width * this.dimension.cell.width;
        const height = this.dimension.grid.height * this.dimension.cell.height;
        const depth = this.dimension.grid.depth * this.dimension.cell.depth;
        const geometry = new THREE.BoxGeometry(width, height, depth, this.dimension.grid.width, this.dimension.grid.height, this.dimension.grid.depth);

        const texture = LoadTexture(EmptyBrick);

        const material = new THREE.MeshBasicMaterial({color: this._color, map: texture});
        const object = new THREE.Group();

        const brick = new THREE.Mesh(geometry, material);
        object.add(brick);

        this.faces.north.visualObject.rotation.y = Math.PI;
        this.faces.east.visualObject.rotation.y = Math.PI / 2;
        this.faces.west.visualObject.rotation.y = 3 * Math.PI / 2;
        this.faces.up.visualObject.rotation.x = -Math.PI / 2;
        object.add(this.faces.north.visualObject);
        object.add(this.faces.east.visualObject);
        object.add(this.faces.south.visualObject);
        object.add(this.faces.west.visualObject);
        object.add(this.faces.up.visualObject);

        object.position.copy(this._orientation.origin);
        // move the brick to place front top left corner at origin
        brick.position.x += width / 2;
        brick.position.y += height / -2;
        brick.position.z += depth / -2;

        return object;
    }

    update(timedelta: number) {
        this.faces.north.update(timedelta);
        this.faces.east.update(timedelta);
        this.faces.south.update(timedelta);
        this.faces.west.update(timedelta);
        this.faces.up.update(timedelta);
    }

}

