import * as THREE from 'three'
import {GLOBAL} from "./global";
import {Visual} from "./visual";
import {saturate} from "../util/colors";
import {Summary, SummaryValue} from "../data/summary";

export {SummaryLeaf}

class SummaryLeaf extends Visual {
    private _source: Summary;
    private _interval: number;
    private _timePassed: number;
    private _canvas: HTMLCanvasElement;
    private _ctx: CanvasRenderingContext2D | null;
    private _valuesTexture: THREE.CanvasTexture;
    private _font: string;
    private _barMaterial: THREE.MeshBasicMaterial;
    private _sumMaterial: THREE.MeshBasicMaterial;
    private _panel: THREE.Mesh | undefined;
    private _sumBars: THREE.Mesh[] | undefined;
    private _bucketBars: THREE.Mesh[] | undefined;

    constructor(source: Summary, color = GLOBAL.COL_WHITE, origin = GLOBAL.origin, up = GLOBAL.up, right = GLOBAL.right) {
        super(origin, up, right, color);
        this._source = source;
        this._interval = 0.5;
        this._timePassed = 0;
        this._canvas = document.createElement('canvas');
        this._ctx = this._canvas.getContext('2d');
        this._canvas.width = 4096;
        this._canvas.height = 2048;
        this._valuesTexture = new THREE.CanvasTexture(this._canvas);
        this._font = '40px Hack Nerd Font Mono';
        this._barMaterial = new THREE.MeshBasicMaterial({color: this._color});
        this._sumMaterial = new THREE.MeshBasicMaterial({color: saturate(this._color, 0.8)});
    }

    protected _getVisual() {
        const geometry = new THREE.PlaneGeometry(this._size.width, this._size.height);

        const material = new THREE.MeshBasicMaterial({color: this._color, map: this._valuesTexture});
        const object = new THREE.Group();

        this._panel = new THREE.Mesh(geometry, material);
        object.add(this._panel);

        object.position.copy(this._orientation.origin);
        // move the pane to place top left corner at origin
        this._panel.position.x += this._size.width / 2;
        this._panel.position.y += this._size.height / -2;

        return object;
    }

    _redrawText(summaryValue: SummaryValue[]) {
        if (this._ctx !== null) {
            const ctx: CanvasRenderingContext2D = this._ctx;
            ctx.clearRect(0, 0, this._canvas.width, this._canvas.height);
            ctx.strokeStyle = 'blue';
            ctx.lineWidth = 1;
            ctx.rect(0, 0, this._canvas.width, this._canvas.height);
            ctx.stroke();
            const bucketCount = summaryValue.length;
            const spacing = this._canvas.width / Math.max(bucketCount, 1);
            let i = 0;
            summaryValue.forEach(el => {
                if (ctx !== null) {
                    this._write(ctx, spacing * i, 2000, el.bucket.toString(), 'rgb(40, 240, 240)', 'rgb(20, 200, 200)');
                    this._write(ctx, spacing * i, 1900, el.value.toString(), 'rgb(240, 240, 240)', 'rgb(200, 200, 200)');
                }
                i = i + 1;
            })
            if (this._panel !== undefined) {
                if (this._panel.material !== undefined) {
                    // @ts-ignore
                    this._panel.material.map.needsUpdate = true;
                }
            }
        }

    }

    _updateBars(summaryValue: SummaryValue[]) {
        const maxValue = Math.max(summaryValue[summaryValue.length - 1].value, 1e-9);
        const numBars = summaryValue.length;
        const spacing = this._size.width / Math.max(numBars, 1);
        if (this._sumBars === undefined) {
            this._sumBars = [];
            this._bucketBars = [];
            let previousValue = 0;
            let barNumber = 0;
            summaryValue.forEach(el => {
                // create panes to represent the bars (1 for the bucket value, and 1 for the cumulated value)
                const pgSum = new THREE.PlaneGeometry(this._size.width / numBars, this._size.height);
                const pgBucket = new THREE.PlaneGeometry(this._size.width / numBars, this._size.height);

                // origins of the planes are shifted from the center to the bottom left corner
                pgSum.translate(0.5 * this._size.width / numBars, 0.5 * this._size.height, 0);
                pgBucket.translate(0.5 * this._size.width / numBars, 0.5 * this._size.height, 0);

                const planeSum = new THREE.Mesh(pgSum, this._sumMaterial);
                const planeBucket = new THREE.Mesh(pgBucket, this._barMaterial);
                const object = new THREE.Group();
                object.add(planeBucket);
                object.add(planeSum);
                this.visualObject.add(object);
                // @ts-ignore
                this._sumBars.push(planeSum);
                // @ts-ignore
                this._bucketBars.push(planeBucket);

                // area origin is at the top left, bars need to be placed at the bottom, spaced out to the right
                object.translateY(-this._size.height * 0.9);
                object.matrixWorldNeedsUpdate = true;

                // put them slightly in front of the area itself, placing the bucket bars in front of the sum bars
                planeSum.position.x = 0.01 * spacing + barNumber * spacing;
                planeSum.position.y = 0;
                planeSum.position.z = GLOBAL.EPSILON;
                planeBucket.position.x = 0.01 * spacing + barNumber * spacing;
                planeBucket.position.y = 0;
                planeBucket.position.z = 2 * GLOBAL.EPSILON;

                planeSum.scale.x = 0.97;
                planeSum.scale.y = 0.89 * el.value / maxValue;
                planeBucket.scale.x = 0.97;
                planeBucket.scale.y = 0.895 * (el.value - previousValue) / maxValue;
                planeBucket.matrixWorldNeedsUpdate = true;
                planeSum.matrixWorldNeedsUpdate = true;

                previousValue = el.value;
                barNumber++;
            })
        } else {
            if (this._sumBars.length !== summaryValue.length) {
                console.error('The number of buckets has changed, this is unsupported at the moment');
                return;
            }
            let previousValue = 0;
            let barNumber = 0;
            summaryValue.forEach(el => {
                // resize the panes to match the value of the bucket and the cumulated value
                if (this._sumBars !== undefined && this._bucketBars !== undefined) {
                    this._sumBars[barNumber].scale.x = 0.97;
                    this._sumBars[barNumber].scale.y = 0.89 * el.value / maxValue;
                    this._bucketBars[barNumber].scale.x = 0.97;
                    this._bucketBars[barNumber].scale.y = 0.895 * (el.value - previousValue) / maxValue;
                    this._bucketBars[barNumber].matrixWorldNeedsUpdate = true;
                    this._sumBars[barNumber].matrixWorldNeedsUpdate = true;
                }
                previousValue = el.value;
                barNumber++;
            })
        }
    }

    _write(context: CanvasRenderingContext2D, x: number, y: number, text: string, fillColor: string, strokeColor: string) {
        context.font = this._font;
        context.fillStyle = fillColor;
        context.fillText(text, x, y);
        context.strokeStyle = strokeColor;
        context.lineWidth = 2;
        context.strokeText(text, x, y);
    }

    update(timedelta: number) {
        this._timePassed += timedelta;
        if (this._timePassed > this._interval) {
            this._timePassed = this._timePassed % this._interval;
            const summaryValue = this._source.read();
            if (summaryValue.length !== 0) {
                this._redrawText(summaryValue);
                this._updateBars(summaryValue);
            }
        }
    }
}