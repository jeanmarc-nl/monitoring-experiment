import * as THREE from 'three'
import {GLOBAL} from "./global";
import {Visual} from "./visual";

export {CarouselLeaf}

class CarouselLeaf extends Visual {
    private _textures: THREE.Texture[];
    private _textureIndex: number;
    private _interval: number;
    private _timePassed: number;
    private _material: THREE.MeshBasicMaterial | undefined;

  constructor(textures: THREE.Texture[], color = GLOBAL.COL_WHITE, origin = GLOBAL.origin, up = GLOBAL.up, right = GLOBAL.right) {
    super(origin, up, right);
    this._textures = textures;
    this._textureIndex = 0;
    this._interval = 1.0;
    this._timePassed = 0;
  }

  protected _getVisual(){
    const geometry = new THREE.PlaneGeometry( this._size.width, this._size.height);

    this._material = new THREE.MeshBasicMaterial( { color: this._color, map: this._textures[this._textureIndex] } );

    const object = new THREE.Group();

    const pane = new THREE.Mesh( geometry, this._material );
    object.add(pane);

    object.position.copy(this._orientation.origin);
    // move the pane to place top left corner at origin
    pane.position.x += this._size.width / 2;
    pane.position.y += this._size.height / -2;

    return object;
  }

  update(timedelta: number) {
    this._timePassed += timedelta;
    if (this._timePassed > this._interval){
      this._timePassed = this._timePassed % this._interval;
      this._textureIndex += 1;
      if (this._textureIndex >= this._textures.length){
        this._textureIndex = this._textureIndex % this._textures.length;
      }
      if (this._material !== undefined) {
          this._material.map = this._textures[this._textureIndex];
      }
    }
  }
}