import * as THREE from 'three'
import {V3} from '../util/vectors'

export {GLOBAL}

const GLOBAL = {
  origin: new THREE.Vector3(0.0, 0.0, 0.0),
  up: new THREE.Vector3(0.0, 1.0, 0.0),
  right: new THREE.Vector3(1.0, 0.0, 0.0),
  PANE_THICKNESS: 0.05,
  EPSILON: 1e-3,
  BP_NORTH: 0,
  BP_EAST: 1,
  BP_SOUTH: 2,
  BP_WEST: 3,
  BP_UP: 4,
  COL_WHITE: new THREE.Color(1, 1, 1),
  UNIT_CUBE: new V3(1, 1, 1)
}
