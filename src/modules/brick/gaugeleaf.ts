import * as THREE from 'three';
import {GLOBAL} from "./global";
import {Visual} from "./visual";
import {Gauge, GaugeValue} from "../data/gauge";
import {LoadTexture} from "../util/textures";
import { RampTemperature} from "../util/colorramp";

export {GaugeLeaf}

let GaugeTex = require('../../resources/images/tiles/metal_floor.png');

class GaugeLeaf extends Visual {
    private _source: Gauge;
    private _interval: number;
    private _timePassed: number;
    private _gauge: THREE.Mesh | undefined;
    private _panel: THREE.Mesh | undefined;
    private _canvas: HTMLCanvasElement;
    private _ctx: CanvasRenderingContext2D | null;
    private _valuesTexture: THREE.CanvasTexture;
    private _text: string[];
    private _font: string;

  constructor(source: Gauge, color = GLOBAL.COL_WHITE, origin = GLOBAL.origin, up = GLOBAL.up, right = GLOBAL.right) {
    super(origin, up, right);
    this._source = source;
    this._interval = 0.5;
    this._timePassed = 0;
    this._canvas = document.createElement('canvas');
    this._ctx = this._canvas.getContext('2d');
    this._canvas.width = 1024;
    this._canvas.height = 512;
    this._valuesTexture = new THREE.CanvasTexture(this._canvas);
    this._text = ['Max: ', 'Val: ', 'Min: '];
    this._font = '80px Hack Nerd Font Mono';
  }

  protected _getVisual(){
    const radius = Math.min(this._size.width, this._size.depth) / 2;
    const geometry = new THREE.CylinderGeometry( radius * 0.9, radius, this._size.height, 24, 1);

    const texture = LoadTexture(GaugeTex);
    texture.repeat.x = 10;
    texture.repeat.y = 5;
    texture.wrapS = THREE.RepeatWrapping;
    texture.wrapT = THREE.RepeatWrapping;

    const material = new THREE.MeshBasicMaterial( { color: this._color, map: texture } );

    const object = new THREE.Group();

    this._gauge = new THREE.Mesh( geometry, material );
    object.add(this._gauge);

    const panelGeometry = new THREE.PlaneGeometry(0.8, 0.2);
    const mat = new THREE.MeshBasicMaterial({color: 0xcccccc, map: this._valuesTexture, alphaTest: 0.5, side: THREE.DoubleSide});
    this._panel = new THREE.Mesh(panelGeometry, mat);
    object.add(this._panel);

    object.position.copy(this._orientation.origin);
    // move the gauge to have the origin at the north west bottom
    this._gauge.position.x = this._size.width / 2;
    this._gauge.position.y = - this._size.height / 2;
    this._gauge.position.z = this._size.depth / 2;
    this._gauge.rotation.x = Math.PI / 2;
    this._gauge.matrixWorldNeedsUpdate = true;


    // move the text to sit upright, at the south west bottom corner

    this._panel.position.x = 0.4;
    this._panel.position.y = -1;
    this._panel.position.z = 0.1;
    this._panel.rotation.x = Math.PI / 2;
    this._panel.matrixWorldNeedsUpdate = true;

    return object;
  }

  _redrawText(gaugeValue: GaugeValue){
      if (this._ctx !== null) {
          this._ctx.clearRect(0, 0, this._canvas.width, this._canvas.height);
          this._ctx.strokeStyle = 'black';
          this._ctx.lineWidth = 1;
          this._ctx.rect(0, 0, this._canvas.width, this._canvas.height);
          this._ctx.stroke();
          this._write(this._ctx, 0, 64, this._text[0] + gaugeValue.max.toString(), 'rgb(240, 40, 40)', 'rgb(200, 20, 20)');
          this._write(this._ctx, 0, 160, this._text[1] + gaugeValue.value.toString(), 'rgb(240, 240, 40)', 'rgb(200, 200, 20)');
          this._write(this._ctx, 0, 254, this._text[2] + gaugeValue.min.toString(), 'rgb(40, 240, 40)', 'rgb(20, 200, 20)');
          if (this._panel !== undefined) {
              if (this._panel.material !== undefined) {
                  // @ts-ignore
                  this._panel.material.map.needsUpdate = true;
              }
          }
      }

  }

  _write(context: CanvasRenderingContext2D, x: number, y: number, text: string, fillColor: string, strokeColor: string){
    context.font = this._font;
    context.fillStyle = fillColor;
    context.fillText(text, x, y);
    context.strokeStyle = strokeColor;
    context.lineWidth = 2;
    context.strokeText(text, x, y);
  }

  update(timedelta: number) {
    this._timePassed += timedelta;
    if (this._timePassed > this._interval){
      this._timePassed = this._timePassed % this._interval;
      const gaugeValue = this._source.read();
      this._redrawText(gaugeValue);
      // map the min to zero, the max to 1, and scale the z-direction to match the current value
      let newScale = 1;
      if (gaugeValue.max != gaugeValue.min) {
        newScale = (gaugeValue.value - gaugeValue.min) / (gaugeValue.max - gaugeValue.min);
      }
      if (this._gauge !== undefined) {
          this._gauge.scale.y = newScale;
          this._gauge.position.z = newScale * this._size.depth / 2;
          this._gauge.matrixWorldNeedsUpdate = true;
          this._color = RampTemperature(newScale);
          // @ts-ignore
          this._gauge.material.color.copy(this._color);
      }
    }
  }
}