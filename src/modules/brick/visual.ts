import * as THREE from 'three'
import {GLOBAL} from "./global";
import {V3} from "../util/vectors";

export {Visual}

interface Orientation {
    origin: THREE.Vector3,
    up: THREE.Vector3,
    right: THREE.Vector3
}

class Visual {
    protected _orientation: Orientation;
    protected _color: THREE.Color;
    protected _size: V3;
    protected _parent: Visual | undefined;
    protected _nodeObject: THREE.Object3D | undefined;

    constructor(origin: THREE.Vector3, up: THREE.Vector3, right: THREE.Vector3, color = GLOBAL.COL_WHITE, size = GLOBAL.UNIT_CUBE) {
        this._orientation = {
            origin: origin,
            up: up,
            right: right
        }
        this._color = color;
        this._size = size;
    }

    get visualObject() {
        if (this._nodeObject === undefined) {
            this._nodeObject = this._getVisual();
        }
        return this._nodeObject;
    }

    get parent() {
        return this._parent;
    }

    setParentAndSize(parent: Visual, size: V3) {
        this._parent = parent;
        this._size = size.clone();
    }

    setPosition(position: THREE.Vector3) {
        this._orientation.origin = position.clone();
    }

    /*
        Derived classes are expected to implement/override the following methods
    */

    addChild(position: V3, size: V3, child: Visual) {
        throw "Adding children not supported for this type of Visual";
    }

    /*
        getVisual creates the Three JS based object(s) representing this Visual
     */
    protected _getVisual(): THREE.Object3D {
        return new THREE.AxesHelper();
    }

    update(timedelta: number) {
        // No-op here, derived classes can use this to update their state for the upcoming frame and/or
        // pass the call on to their children
    }
}
