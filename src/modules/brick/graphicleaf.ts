import * as THREE from 'three'
import {GLOBAL} from "./global";
import {Visual} from "./visual";

export {GraphicLeaf}

class GraphicLeaf extends Visual {
    private _texture: THREE.Texture;
    constructor(texture: THREE.Texture, color = GLOBAL.COL_WHITE, origin = GLOBAL.origin, up = GLOBAL.up, right = GLOBAL.right) {
        super(origin, up, right);
        this._texture = texture;
    }

    protected _getVisual() {
        const geometry = new THREE.PlaneGeometry(this._size.width, this._size.height);

        const material = new THREE.MeshBasicMaterial({color: this._color, map: this._texture});
        const object = new THREE.Group();

        const pane = new THREE.Mesh(geometry, material);
        object.add(pane);

        object.position.copy(this._orientation.origin);
        // move the pane to place top left corner at origin
        pane.position.x += this._size.width / 2;
        pane.position.y += this._size.height / -2;

        return object;
    }
}