import * as THREE from 'three';
import {GLOBAL} from "./global";
import {Visual} from "./visual";
import {LoadTexture} from "../util/textures";
import {V2, V3} from "../util/vectors";

let EmptyTile = require('../../resources/images/tiles/empty.png');

export {Grid, GridDimension}

class GridDimension {
    public grid: V2;
    public cell: V2;
    constructor(cols: number, rows: number, cellWidth = 1, cellHeight = 1) {
        this.grid = new V2(cols, rows);
        this.cell = new V2(cellWidth, cellHeight);
    }

    clone() {
        return new GridDimension(this.grid.width, this.grid.height, this.cell.width, this.cell.height);
    }
}

/*
    A Grid represents a rectangular area of cells that keeps track of the elements placed on it
 */
class Grid extends Visual {
    public dimension: GridDimension;
    private _children: Visual[];
    constructor(gridDimension: GridDimension, origin = GLOBAL.origin, up = GLOBAL.up, right = GLOBAL.right) {
        super(origin, up, right);
        this.dimension = gridDimension.clone();
        this._children = [];
    }

    addChild(position: V3, size: V3, child: Visual) {
        const childSize = size.clone();
        childSize.times(new V3(this.dimension.cell.width, this.dimension.cell.height, 1));
        child.setParentAndSize(this, childSize);

        const childOrigin = new THREE.Vector3(
            position.x * this.dimension.cell.width,
            -position.y * this.dimension.cell.width,
            (GLOBAL.PANE_THICKNESS / 2) + GLOBAL.EPSILON
        );
        child.setPosition(childOrigin);
        this.visualObject.add(child.visualObject);
        this._children.push(child);

    }

    protected _getVisual() {
        const width = this.dimension.grid.width * this.dimension.cell.width;
        const height = this.dimension.grid.height * this.dimension.cell.height;
        const geometry = new THREE.BoxGeometry(width, height, GLOBAL.PANE_THICKNESS);

        const texture = LoadTexture(EmptyTile);
        texture.wrapS = THREE.RepeatWrapping;
        texture.wrapT = THREE.RepeatWrapping;
        texture.repeat.set(this.dimension.grid.width, this.dimension.grid.height);

        const material = new THREE.MeshBasicMaterial({color: this._color, map: texture});
        const object = new THREE.Group();

        const pane = new THREE.Mesh(geometry, material);
        object.add(pane);

        object.position.copy(this._orientation.origin);
        // move the pane to place top left corner at origin
        pane.position.x += width / 2;
        pane.position.y += height / -2;

        return object;
    }

    setParentAndSize(parent: Visual, size: V3) {
        super.setParentAndSize(parent, size);
        // update cell size based on available size
        this.dimension.cell.width = size.x / this.dimension.grid.width;
        this.dimension.cell.height = size.y / this.dimension.grid.height;
    }

    update(timedelta: number) {
        this._children.forEach(function (obj) {
            obj.update(timedelta)
        });
    }
}