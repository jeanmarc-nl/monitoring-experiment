export {V2, V3}

class V2 {
    public v: number[];

    constructor(x: number, y: number) {
        this.v = [x, y];
    }

    get x() {
        return this.v[0];
    }

    get y() {
        return this.v[1];
    }

    get width() {
        return this.v[0];
    }

    get height() {
        return this.v[1];
    }

    set x(num) {
        this.v[0] = num;
    }

    set y(num) {
        this.v[1] = num;
    }

    set width(num) {
        this.v[0] = num;
    }

    set height(num) {
        this.v[1] = num;
    }

    clone() {
        return new V2(this.x, this.y);
    }

    times(that: V2) {
        this.v[0] *= that.x;
        this.v[1] *= that.y;
    }
}

class V3 {
    public v: number[];

    constructor(x: number, y: number, z: number) {
        this.v = [x, y, z];
    }

    get x() {
        return this.v[0];
    }

    get y() {
        return this.v[1];
    }

    get z() {
        return this.v[2];
    }

    get width() {
        return this.v[0];
    }

    get height() {
        return this.v[1];
    }

    get depth() {
        return this.v[2];
    }

    set x(num) {
        this.v[0] = num;
    }

    set y(num) {
        this.v[1] = num;
    }

    set z(num) {
        this.v[2] = num;
    }

    set width(num) {
        this.v[0] = num;
    }

    set height(num) {
        this.v[1] = num;
    }

    set depth(num) {
        this.v[2] = num;
    }

    clone() {
        return new V3(this.x, this.y, this.z);
    }

    times(that: V3) {
        this.v[0] *= that.x;
        this.v[1] *= that.y;
        this.v[2] *= that.z;
    }
}
