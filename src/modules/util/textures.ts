import * as THREE from 'three'

let UVTest = require('../../resources/images/tiles/uv-test.png');

export { LoadTexture, LoadUVTestTexture }

const txLoader = new THREE.TextureLoader();

function LoadTexture(textureRef: string): THREE.Texture {
  const tx = txLoader.load(textureRef);
  return tx;
}

function LoadUVTestTexture(){
  return LoadTexture(UVTest);
}
