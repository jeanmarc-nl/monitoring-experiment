import * as THREE from 'three'

export {saturate}

function saturate(color: THREE.Color, factor: number){
  return new THREE.Color(color.r * factor, color.g * factor, color.b * factor);
}
