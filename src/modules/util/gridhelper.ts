import * as THREE from 'three';
import { GUI } from 'dat.gui';

// Turns both axes and grid visible on/off
// dat.GUI requires a property that returns a bool
// to decide to make a checkbox so we make a setter
// and getter for `visible` which we can tell dat.GUI
// to look at.
export class AxisGridHelper {
    private grid: THREE.GridHelper;
    private axes: THREE.AxesHelper;
    private _visible: boolean;

    constructor(node: THREE.Object3D, units = 10, plane = 'xz') {
        this.axes = new THREE.AxesHelper();
        // @ts-ignore
        this.axes.material.depthTest = false;
        this.axes.renderOrder = 2;  // after the grid
        node.add(this.axes);

        this.grid = new THREE.GridHelper(units, units);
        // @ts-ignore
        this.grid.material.depthTest = false;
        this.grid.renderOrder = 1;
        node.add(this.grid);

        if (plane == 'xy') {
            this.grid.rotateX(Math.PI / 2);
        } else if (plane == 'yz') {
            this.grid.rotateZ(Math.PI / 2);
        }

        this._visible = false;
        this.visible = false;
    }

    get visible() {
        return this._visible;
    }

    set visible(v) {
        this._visible = v;
        this.grid.visible = v;
        this.axes.visible = v;
    }
}

export function makeAxisGrid(gui: GUI, node: THREE.Object3D, label: string, units: number, plane = 'xz') {
    const helper = new AxisGridHelper(node, units, plane);
    gui.add(helper, 'visible').name(label);
}
