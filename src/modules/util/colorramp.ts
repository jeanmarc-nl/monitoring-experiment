import * as THREE from 'three'

export {RampPBGR, RampPBGRP, RampFull, RampTemperature}

// value clamped to [0, 1], returns RGB value on spectrum black-purple-blue-green-yellow-orange-red-white
function RampFull(value: number) {
    return sampleGradient(GRAD_BPBGRW, value);
}

// value clamped to [0, 1], returns RGB value on spectrum purple-blue-green-yellow-orange-red
function RampPBGR(value: number) {
    return sampleGradient(GRAD_PBGR, value);
}

// value is used modulo scale, returns RGB value on rainbow spectrum including back to purple at the limit
// this allows a smooth repeat of the ramp
function RampPBGRP(value: number, scale = 1, offset = 0) {
    value = (value - offset) % scale;
    return sampleGradient(GRAD_PBGRP, value);
}

function RampTemperature(value: number) {
    return sampleGradient(GRAD_TEMPERATURE, value);
}

function sampleGradient(gradient: GradientStop[], value: number) {
    value = Math.min(Math.max(value, 0), 1);

    let result = getColor(gradient, value);
    return result;
}

function getColor(array: GradientStop[], value: number) {
    const color = new THREE.Color(0, 0, 0);
    const res = getInterval(array, value);
    color.lerpColors(array[res.first].color, array[res.last].color, (value - array[res.first].le) / (array[res.last].le - array[res.first].le));
    return color;
}

interface Stops {
    first: number,
    last: number
}

function getInterval(array: GradientStop[], value: number): Stops {
    let i = 0;
    const end = array.length - 1;
    for (i = 0; i < end; i++) {
        if (array[i].le <= value && value <= array[i + 1].le) {
            return {first: i, last: i + 1};
        }
    }
    return {first: 0, last: array.length - 1};
}


interface GradientStop {
    le: number,
    color: THREE.Color
}

const GRAD_BPBGRW: GradientStop[] = [
    {le: 0.0, color: new THREE.Color(0, 0, 0)},
    {le: 1 / 7, color: new THREE.Color(1, 0, 1)},
    {le: 2 / 7, color: new THREE.Color(0, 0, 1)},
    {le: 3 / 7, color: new THREE.Color(0, 1, 1)},
    {le: 4 / 7, color: new THREE.Color(0, 1, 0)},
    {le: 5 / 7, color: new THREE.Color(1, 1, 0)},
    {le: 6 / 7, color: new THREE.Color(1, 0, 0)},
    {le: 1.0, color: new THREE.Color(1, 1, 1)}
];

const GRAD_TEMPERATURE: GradientStop[] = [
    {le: 0.0, color: new THREE.Color(0.1, 0.1, 0.1)},
    {le: 1 / 6, color: new THREE.Color(0.1, 0.1, 0.8)},
    {le: 2 / 6, color: new THREE.Color(0.1, 0.8, 0.8)},
    {le: 3 / 6, color: new THREE.Color(0.1, 0.8, 0.1)},
    {le: 4 / 6, color: new THREE.Color(0.8, 0.8, 0.1)},
    {le: 5 / 6, color: new THREE.Color(0.8, 0.1, 0.1)},
    {le: 1.0, color: new THREE.Color(0.8, 0.8, 0.8)}
];

const GRAD_PBGR: GradientStop[] = [
    {le: 0.0, color: new THREE.Color(1, 0, 1)},
    {le: 0.2, color: new THREE.Color(0, 0, 1)},
    {le: 0.4, color: new THREE.Color(0, 1, 1)},
    {le: 0.6, color: new THREE.Color(0, 1, 0)},
    {le: 0.8, color: new THREE.Color(1, 1, 0)},
    {le: 1.0, color: new THREE.Color(1, 0, 0)}
];
const GRAD_PBGRP: GradientStop[] = [
    {le: 0.0, color: new THREE.Color(1, 0, 1)},
    {le: 1 / 6, color: new THREE.Color(0, 0, 1)},
    {le: 2 / 6, color: new THREE.Color(0, 1, 1)},
    {le: 3 / 6, color: new THREE.Color(0, 1, 0)},
    {le: 4 / 6, color: new THREE.Color(1, 1, 0)},
    {le: 5 / 6, color: new THREE.Color(1, 0, 0)},
    {le: 1.0, color: new THREE.Color(1, 0, 1)}
];
