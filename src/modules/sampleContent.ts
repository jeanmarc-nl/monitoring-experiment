// @ts-nocheck
import * as THREE from 'three';

// @ts-ignore
import { Interaction } from 'three.interaction';

import Gauge from "../resources/images/dial_gauge2.png";
import Floor from "../resources/images/tiles/metal_floor.png";
import {makeAxisGrid} from "./util/gridhelper";


let textCanvas = {};
let theControl = {};
let ctx = {};
let font1 = '48px monospace';
let font2 = '96px monospace';
let floorText = '<= Hello world >=';
let timetext = "";
let floor = {};

function redrawText(){
  ctx.clearRect(0, 0, textCanvas.width, textCanvas.height);
  ctx.strokeStyle = 'black';
  ctx.lineWidth = 1;
  ctx.rect(0, 0, textCanvas.width, textCanvas.height);
  ctx.stroke();
  ctx.font = font1;
  ctx.fillStyle = 'rgb(200,100,100)';
  ctx.fillText( floorText, 0, 50);
  ctx.strokeStyle = 'rgb(100, 50, 50)';
  ctx.lineWidth = 2;
  ctx.strokeText( floorText, 0, 50);
  ctx.fillStyle = 'rgb(240,240,80)';
  ctx.fillText(floorText, 0, 240);
  ctx.strokeStyle = 'rgb(220,220,50)';
  ctx.strokeText( floorText, 0, 240);

  ctx.font = font2;
  ctx.fillStyle = 'cyan';
  ctx.fillText(timetext, 0, 150);
  ctx.strokeStyle = 'blue';
  ctx.lineWidth = 2;
  ctx.strokeText(timetext, 0, 150);
}

export function addDemoContent(renderer, scene, camera, controls){
  const gauge_texture = new THREE.TextureLoader().load(Gauge);

  const texture = new THREE.TextureLoader().load(Floor);
  texture.wrapS = THREE.RepeatWrapping;
  texture.wrapT = THREE.RepeatWrapping;
  texture.repeat.set( 20, 20 );

  textCanvas = document.createElement('canvas');
  //document.body.appendChild(textCanvas);
  ctx = textCanvas.getContext('2d');
  textCanvas.width = 512;
  textCanvas.height = 256;
  redrawText();
  const textTx = new THREE.CanvasTexture(textCanvas);
  textTx.wrapS = THREE.RepeatWrapping;
  textTx.wrapT = THREE.RepeatWrapping;
  textTx.repeat.set( 10, 20 );

  document.fonts.onloadingdone = function (fontFaceSetEvent){
    console.log('fonts loaded, update demo font');
    console.log(fontFaceSetEvent);
    font1 = '48px Hack Nerd Font Mono';
    font2 = '96px Hack Nerd Font Mono';

    redrawText();
    if (floor.material !== undefined){
      floor.material.map.needsUpdate = true;
    }
  }

  const floor_geometry = new THREE.BoxGeometry( 20, 0.01, 20 );
  //const floor_material = new THREE.MeshBasicMaterial( { color: 0xcccccc, map: textTx, alphaTest: 0.4 } );
  const floor_material = new THREE.MeshBasicMaterial( { color: 0xcccccc, map: texture, alphaTest: 0.4 } );

  const geometry = new THREE.BoxGeometry();
  const material = new THREE.MeshBasicMaterial( { color: 0xcccccc, map: gauge_texture, alphaTest: 0.5, side: THREE.DoubleSide } );
  cube = new THREE.Mesh( geometry, material );
  floor = new THREE.Mesh( floor_geometry, floor_material );
  scene.add( cube );
  scene.add( floor );

  cube.scale.setScalar(0.06);
  floor.position.y=-7;
  floor.position.z=10;

  //makeAxisGrid(gui, floor, 'floor', 12);
  //makeAxisGrid(gui, cube, 'cube', 4);

  // new a interaction, then you can add interaction-event with your free style
  const interaction = new Interaction(renderer, scene, camera);

  cube.cursor = 'pointer';
  cube.on('click', function(ev) { rot_delta *= -1; console.log(font1)});
  cube.on('mouseover', function(ev) { showGrab();})
  cube.on('mouseout', function(ev) { clearGrab();})
  theControl = controls;

  // keep a reference to the DOMElement, so we can set the mouse pointer
  rendererDomElement = renderer.domElement;
}

function showGrab(){
  //console.log('showGrab');
  rendererDomElement.classList.add('grab');
}

function clearGrab(){
  //console.log('clearGrab');
  rendererDomElement.classList.remove('grab');
}

let rendererDomElement = {};
let cube = {};

let rot_delta = 0.01;
let total_time = 0;
let last_second = 0;

export function updateDemoScene(timeDelta){
  total_time += timeDelta;
  if (Math.floor(total_time) > last_second){
    last_second = Math.floor(total_time);
    timetext = last_second.toString();
    redrawText();
    if (floor.material !== undefined){
      floor.material.map.needsUpdate = true;
    }
  }

  // make sure cube is located at controls lookat point
  cube.position.copy(theControl.target);

  cube.rotation.x += rot_delta;
  cube.rotation.y += rot_delta;
}